pub fn set_lower(num: u16, lower: u8) -> u16 {
    (num & 0xFF00) | lower as u16
}

pub fn set_upper(num: u16, upper: u8) -> u16 {
    (num & 0x00FF) + ((upper as u16) << 8)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_set_lower() {
        let mut num = 0xFF69;
        let lower = 0x15;

        num = set_lower(num, lower);

        assert_eq!(num, 0xFF15);
    }

    #[test]
    fn test_set_upper() {
        let mut num = 0x69FF;
        let upper = 0x15;

        num = set_upper(num, upper);

        assert_eq!(num, 0x15FF);
    }
}
