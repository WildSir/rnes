use crate::{cpu::Cycle, mapper::MapperTrait as Mapper};

#[derive(Debug)]
pub struct Nrom {
    prg_ram: [u8; 0x2000],
    prg_rom1: [u8; 0x4000],
    prg_rom2: Option<[u8; 0x4000]>,
}

impl Nrom {
    pub fn new() -> Self {
        Nrom {
            prg_ram: [0; 0x2000],
            prg_rom1: [0; 0x4000],
            prg_rom2: None,
        }
    }
    // TOOD: Add method to trait
    pub fn load_rom(&mut self, bank1: [u8; 0x4000], bank2: Option<[u8; 0x4000]>) {
        self.prg_rom1 = bank1;
        if let Some(bank2) = bank2 {
            self.prg_rom2 = Some(bank2);
        }
    }
}

impl Mapper for Nrom {
    fn request_raw(&mut self, addr_bus: u16, data_bus: &mut u8, control_bus: Cycle) {
        match control_bus {
            Cycle::Read => {
                *data_bus = match addr_bus {
                    0x4020..=0x5FFF => *data_bus,
                    // TODO: Famicom mode or similar
                    0x6000..=0x7FFF => self.prg_ram[(addr_bus - 0x6000) as usize],
                    0x8000..=0xBFFF => self.prg_rom1[(addr_bus - 0x8000) as usize],
                    0xC000..=0xFFFF => {
                        if let Some(prg_rom2) = self.prg_rom2 {
                            prg_rom2[(addr_bus - 0xC000) as usize]
                        } else {
                            self.prg_rom1[(addr_bus - 0xC000) as usize]
                        }
                    }
                    _ => unreachable!(),
                }
            }
            Cycle::Write => match addr_bus {
                0x4020..=0x5FFF => {}
                // TODO: Famicom mode or similar
                0x6000..=0x7FFF => self.prg_ram[(addr_bus - 0x6000) as usize] = *data_bus,
                0x8000..=0xBFFF => self.prg_rom1[(addr_bus - 0x8000) as usize] = *data_bus,
                0xC000..=0xFFFF => {
                    if let Some(mut prg_rom2) = self.prg_rom2 {
                        prg_rom2[(addr_bus - 0xC000) as usize] = *data_bus;
                    } else {
                        self.prg_rom1[(addr_bus - 0xC000) as usize] = *data_bus;
                    }
                }
                _ => unreachable!(),
            },
        };
    }
}
