#![allow(dead_code)]

mod bus;
mod cpu;
mod mapper;
mod utils;

pub use cpu::Cpu;

use crate::mapper::Mapper;
use anyhow::{bail, Context, Result};
use std::{env, fs};

fn main() -> Result<()> {
    env_logger::init();
    let args: Vec<String> = env::args().into_iter().collect();

    let Some(rom_dir) = args.get(1) else {
        bail!("No ROM specified")
    };

    let mut cpu = Cpu::new();
    let bank1 = {
        let mut file_contents = fs::read(rom_dir).context("Failed to read ROM")?;
        file_contents.resize(16384, 0);
        file_contents.try_into().unwrap()
    };

    match cpu.bus.mapper {
        Mapper::Nrom(ref mut nrom) => nrom.load_rom(bank1, None),
    }
    
    println!();

    cpu.reset();

    cpu.execute_instruction();
    cpu.execute_instruction();

    println!();
    
    Ok(())
}
