mod instruction;

use bitflags::bitflags;

use crate::{
    bus::Bus,
    cpu::instruction::{
        AddressingMode::{self, *},
        Instruction::{self, *},
    },
    utils,
};

bitflags! {
    #[derive(Debug, Clone, Copy)]
    pub struct Status: u8 {
        /// Carry Flag
        const C = 0b00000001;
        /// Zero Flag
        const Z = 0b00000010;
        /// Interrupt Disable
        const I = 0b00000100;
        /// Decimal Mode
        const D = 0b00001000;
        /// Break Command
        const B = 0b00010000;
        const _ = 0b00100000;
        /// Overflow Flag
        const V = 0b01000000;
        /// Negative Flag
        const N = 0b10000000;
    }
}

#[derive(Debug)]
pub enum Cycle {
    Read,
    Write,
}

#[derive(Debug, Clone)]
pub enum Interrupt {
    Irq,
    Nmi,
    Reset,
}
impl Interrupt {
    const RESET_VECTOR: u16 = 0xFFFC;
    const IRQ_VECTOR: u16 = 0xFFFE;
    const NMI_VECTOR: u16 = 0xFFFA;

    fn get_vec_addr(&self) -> u16 {
        match &self {
            Interrupt::Irq => Self::IRQ_VECTOR,
            Interrupt::Nmi => Self::NMI_VECTOR,
            Interrupt::Reset => Self::RESET_VECTOR,
        }
    }
}

#[derive(Debug)]
pub struct Cpu {
    /// The amount of cycles ran
    pub cycles: usize,
    master_clock: usize,
    /// Current cycle
    pub current_cycle: Option<Cycle>,
    /// PC
    pub pc: u16,
    /// SP
    pub sp: u8,
    /// A
    pub acc: u8,
    /// Index Register X
    pub x: u8,
    /// Index Register Y
    pub y: u8,
    /// Processor Status (P)
    pub status: Status,
    /// Used to communicate with the NES's memory map
    pub bus: Bus,

    // Active low
    pub irq_input: bool,
    /// Whether an IRQ was polled in the previous cycle's φ1
    irq_detected: bool,

    // Active low (high to low)
    pub nmi_input: bool,
    prev_nmi: bool,
    /// Whether a NMI was polled in the previous cycle's φ1
    nmi_detected: bool,

    // Active low
    pub reset_input: bool,
    reset_detected: bool,

    /// Used by a few methods to determine the behavior they should follow
    isr_running: Option<Interrupt>,

    /// Which interrupt is waiting to be executed
    interrupt_pending: Option<Interrupt>,

    // Stores which instruction is currently being processed
    instruction_reg: Option<Instruction>,
    // Stores the addressing mode of the current instruction for convinience
    addr_mode_reg: Option<AddressingMode>,
}

impl Cpu {
    const NTSC_MASTER_CLOCK_FREQ: f32 = 21_477_272.0;
    const NTSC_DIVISOR: f32 = 12.0;
    const NTSC_CPU_CLOCK_FREQ: f32 = Self::NTSC_MASTER_CLOCK_FREQ / Self::NTSC_DIVISOR;

    const STACK: u16 = 0x100;

    // https://www.nesdev.org/wiki/CPU_power_up_state
    /// Create a new instance of the CPU
    pub fn new() -> Cpu {
        #[allow(unreachable_code)]
        Cpu {
            cycles: 0,
            current_cycle: None,
            pc: 0,
            sp: 0,
            acc: 0,
            x: 0,
            y: 0,
            status: Status::from_bits_retain(0b00100000),
            bus: Bus::new(),
            instruction_reg: None,
            addr_mode_reg: None,
            master_clock: 0,
            irq_input: true,
            irq_detected: false,
            interrupt_pending: None,
            isr_running: None,
            nmi_input: true,
            prev_nmi: true,
            nmi_detected: false,
            reset_input: true,
            reset_detected: false,
        }
    }

    // NOTE: Reset does not modify the stack, but does increment the stack pointer
    // #  address R/W description
    // --- ------- --- -----------------------------------------------
    // 1    PC     R  fetch opcode (and discard it - $00 (BRK) is forced into the opcode register instead)
    // 2    PC     R  read next instruction byte (actually the same as above, since PC increment is suppressed. Also discarded.)
    // 3  $0100,S  W  push PCH on stack, decrement S
    // 4  $0100,S  W  push PCL on stack, decrement S
    //*** At this point, the signal status determines which interrupt vector is used ***
    // 5  $0100,S  W  push P on stack (with B flag *clear*), decrement S
    // 6   A       R  fetch PCL (A = FFFE for IRQ, A = FFFA for NMI), set I flag
    // 7   A       R  fetch PCH (A = FFFF for IRQ, A = FFFB for NMI)
    pub fn reset(&mut self) {
        // TODO: Should I reset the registers and such?
        // TODO: The rest

        println!("CYCLE ADDR  DATA  R/W   PC    ACC   NV1BDIZC  X     Y     SP    INT    IR    ");
        println!("----------------------------------------------------------------------------------");
        self.interrupt_pending = Some(Interrupt::Reset);
        self.execute_instruction();
    }

    /// Fetches the opcode, updating the instruction and addressing mode registers; reads the first operand; handles interrupts if they are pending
    pub fn fetch_opcode(&mut self) {
        let mut opcode = self.read(self.pc);

        // ...
        //  1    PC     R  fetch opcode (and discard it - $00 (BRK) is forced into the opcode register instead)
        // ...
        if self.isr_running.is_some() {
            opcode = 0x00;
        }

        // If this is a IRQ or NMI interrupt, the PC increment should be suppressed:
        // ...
        // 2    PC     R  read next instruction byte (actually the same as above, since PC increment is suppressed. Also discarded.)
        // ...
        if self.isr_running.is_none() {
            self.pc += 1;
        }

        self.lookup_opcode(opcode);

        // Stores the address on the data bus
        self.read(self.pc);
        if self.isr_running.is_none() {
            self.pc += 1;
        }
    }

    pub fn read(&mut self, addr: u16) -> u8 {
        self.start_cycle_raw(self.isr_running.is_none());
        self.bus.control_bus = Cycle::Read;
        self.bus.addr_bus = addr;
        self.end_cycle();
        self.bus.data_bus
    }

    pub fn write(&mut self, addr: u16, data: u8) {
        self.start_cycle_raw(self.isr_running.is_none());
        self.bus.control_bus = Cycle::Write;
        self.bus.addr_bus = addr;
        self.bus.data_bus = data;
        self.end_cycle();
    }

    pub fn fetch_data(&mut self) -> u8 {
        match self.addr_mode_reg.as_ref().unwrap() {
            Accumulator => self.acc,
            Immediate => self.bus.data_bus,
            ZeroPage => self.read(self.bus.data_bus as u16),
            ZeroPageX => {
                self.read(self.bus.data_bus as u16);
                self.read((self.bus.addr_bus as u8).wrapping_add(self.x) as u16)
            }
            ZeroPageY => {
                self.read(self.bus.data_bus as u16);
                self.read((self.bus.addr_bus as u8).wrapping_add(self.y) as u16)
            }
            Absolute => {
                let addr_lsb = self.bus.data_bus;
                self.read(self.pc);
                self.pc += 1;
                self.read(u16::from_le_bytes([addr_lsb, self.bus.data_bus]))
            }
            AbsoluteX => {
                let (addr_lsb, carry) = self.bus.data_bus.overflowing_add(self.x);
                self.read(self.pc);
                self.pc += 1;
                let addr_msb = self.bus.data_bus;

                self.read(u16::from_le_bytes([addr_lsb, addr_msb]));
                if carry {
                    self.read(u16::from_le_bytes([addr_lsb, addr_msb.wrapping_add(1)]));
                }
                self.bus.data_bus
            }
            AbsoluteY => {
                let (addr_lsb, carry) = self.bus.data_bus.overflowing_add(self.y);
                self.read(self.pc);
                self.pc += 1;
                let addr_msb = self.bus.data_bus;

                self.read(u16::from_le_bytes([addr_lsb, addr_msb]));
                if carry {
                    self.read(u16::from_le_bytes([addr_lsb, addr_msb.wrapping_add(1)]));
                }
                self.bus.data_bus
            }
            IndirectX => {
                self.read(self.bus.data_bus as u16); // Cycle 3
                let addr_lsb = self.read((self.bus.addr_bus as u8).wrapping_add(self.x) as u16); // Cycle 4
                let addr_msb = self.read((self.bus.addr_bus as u8).wrapping_add(1) as u16); // Cycle 5
                self.read(u16::from_le_bytes([addr_lsb, addr_msb])) // Cycle 6
            }
            IndirectY => {
                let (addr_lsb, carry) = self.read(self.bus.data_bus as u16).overflowing_add(self.y); // Cycle 3
                let addr_msb = self.read((self.bus.addr_bus as u8).wrapping_add(1) as u16); // Cycle 4
                self.read(u16::from_le_bytes([addr_lsb, addr_msb])); // Cycle 5
                if carry {
                    self.read(u16::from_le_bytes([addr_lsb, addr_msb.wrapping_add(1)]));
                }
                self.bus.data_bus
            }
            _ => unimplemented!(),
        }
    }

    pub fn write_data(&mut self, data: u8) {
        match self.addr_mode_reg.as_ref().unwrap() {
            Accumulator => self.acc = data,
            ZeroPage => self.write(self.bus.data_bus as u16, data),
            ZeroPageX => {
                self.read(self.bus.data_bus as u16);
                self.write((self.bus.addr_bus as u8).wrapping_add(self.x) as u16, data);
            }
            ZeroPageY => {
                self.read(self.bus.data_bus as u16);
                self.write((self.bus.addr_bus as u8).wrapping_add(self.y) as u16, data);
            }
            Absolute => {
                let addr_lsb = self.bus.data_bus;
                self.read(self.pc);
                self.pc += 1;
                self.read(u16::from_le_bytes([addr_lsb, self.bus.data_bus]));
                self.write(self.bus.addr_bus, data);
            }
            AbsoluteX => {
                let (addr_lsb, carry) = self.bus.data_bus.overflowing_add(self.x);
                self.read(self.pc);
                self.pc += 1;
                self.read(u16::from_le_bytes([addr_lsb, self.bus.data_bus]));
                self.write(
                    u16::from_le_bytes([addr_lsb, self.bus.data_bus + carry as u8]),
                    data,
                );
            }
            AbsoluteY => {
                let (addr_lsb, carry) = self.bus.data_bus.overflowing_add(self.y);
                self.read(self.pc);
                self.pc += 1;
                self.read(u16::from_le_bytes([addr_lsb, self.bus.data_bus]));
                self.write(
                    u16::from_le_bytes([addr_lsb, self.bus.data_bus + carry as u8]),
                    data,
                );
            }
            IndirectX => {
                self.read(self.bus.data_bus as u16);
                let addr_lsb = self.read((self.bus.addr_bus as u8).wrapping_add(self.x) as u16); // Cycle 3
                let addr_msb = self.read((self.bus.addr_bus as u8).wrapping_add(1) as u16); // Cycle 4
                self.write(u16::from_le_bytes([addr_lsb, addr_msb]), data);
            }
            IndirectY => {
                let (addr_lsb, carry) = self.read(self.bus.data_bus as u16).overflowing_add(self.y);
                let addr_msb = self.read((self.bus.addr_bus as u8).wrapping_add(1) as u16);
                self.read(u16::from_le_bytes([addr_lsb, addr_msb]));
                self.write(
                    u16::from_le_bytes([addr_lsb, addr_msb.wrapping_add(carry as u8)]),
                    data,
                );
            }
            _ => unimplemented!(),
        }
    }

    pub fn execute_instruction(&mut self) {
        self.handle_interrupt();
        self.fetch_opcode();
        if let Some(instruction) = &self.instruction_reg {
            match instruction {
                NOP => {}
                LDA => self.lda(),
                LDX => self.ldx(),
                LDY => self.ldy(),
                BRK => self.brk(),
                _ => todo!(),
            }
        };
    }

    /// Push a value to the stack and decrement the stack pointer
    fn push_stack(&mut self, data: u8) {
        match self.instruction_reg.as_ref().unwrap() {
            // #  address R/W description
            // --- ------- --- -----------------------------------------------
            // 1    PC     R  fetch opcode, increment PC
            // 2    PC     R  read next instruction byte (and throw it away),
            //                increment PC
            // 3  $0100,S  W  push PCH on stack, decrement S
            // 4  $0100,S  W  push PCL on stack, decrement S
            //*** At this point, the signal status determines which interrupt vector is used ***
            // 5  $0100,S  W  push P on stack (with B flag set), decrement S
            // 6   $FFFE   R  fetch PCL, set I flag
            // 7   $FFFF   R  fetch PCH
            BRK => {
                if let Some(Interrupt::Reset) = self.isr_running {
                    // "A reset also goes through the same sequence, but suppresses writes, decrementing the stack pointer thrice without modifying memory. This is why the I flag is always set on reset."
                    self.read(Self::STACK + self.sp as u16);
                } else {
                    self.write(Self::STACK + self.sp as u16, data);
                }

                self.sp = self.sp.wrapping_sub(1);
            }
            // TODO
            _ => unreachable!(),
        }
    }

    fn pull_stack(&mut self) {
        match self.instruction_reg.as_ref().unwrap() {
            RTI => {
                self.status = Status::from_bits_retain(self.read(Self::STACK + self.sp as u16)); // Cycle 3
                self.sp += 1;

                self.pc = utils::set_lower(self.pc, self.read(Self::STACK + self.sp as u16)); // Cycle 4
                self.sp += 1;

                self.pc = utils::set_upper(self.pc, self.read(Self::STACK + self.sp as u16)); // Cycle 5
                self.sp += 1;

                self.read(self.pc); // Cycle 6
                self.pc += 1;
            }
            _ => unreachable!(),
        }
    }

    fn start_cycle_raw(&mut self, interrupt_polling: bool) {
        self.master_clock += Self::NTSC_DIVISOR as usize / 2;
        // phi 1
        if interrupt_polling {
            self.set_interrupts();
        }
    }

    fn start_cycle(&mut self) {
        self.start_cycle_raw(true)
    }

    fn end_cycle(&mut self) {
        // TODO: Wait to make sure the first half (phi 1) has finished, so make sure 6 master clock cycles have passed
        self.master_clock += Self::NTSC_DIVISOR as usize / 2;
        // phi 2
        self.poll_interrupts();

        self.bus.request_raw();

        self.cycles += 1;
        println!(
            "{:5} {:5} {:5} {:5} {:5} {:5} {:9} {:5} {:5} {:5} {:6} {:5}",
            format!("{:04}", self.cycles),
            format!("{:04X}", self.bus.addr_bus),
            format!("{:02X}", self.bus.data_bus),
            &format!("{:?}", self.bus.control_bus)[0..1],
            format!("{:04X}", self.pc),
            format!("{:02X}", self.acc),
            format!("{:08b}", self.status.bits()),
            format!("{:02X}", self.x),
            format!("{:02X}", self.y),
            format!("{:02X}", self.sp),
            self.isr_running.clone().map_or_else(|| String::from("None"), |isr| format!("{:?}", isr).to_uppercase()),
            self.instruction_reg.clone().map_or_else(|| String::from("N/A"), |instr| format!("{:?}", instr).to_uppercase()),
        );
    }

    fn poll_interrupts(&mut self) {
        if !self.irq_input {
            self.irq_detected = true;
        } else {
            self.irq_detected = false;
        }
        if !self.nmi_input && self.prev_nmi {
            self.nmi_detected = true;
        } else {
            self.nmi_detected = false;
        }

        self.prev_nmi = self.nmi_input;

        // TODO: Figure out the actual behavior of resetting
        if !self.reset_input {
            self.reset_detected = true;
        } else {
            self.reset_detected = false;
        }
    }

    fn set_interrupts(&mut self) {
        if self.reset_detected {
            self.interrupt_pending = Some(Interrupt::Reset);
            return;
        }
        if self.nmi_detected {
            self.interrupt_pending = Some(Interrupt::Nmi);
            return;
        }
        if self.irq_detected && !self.status.contains(Status::I) {
            self.interrupt_pending = Some(Interrupt::Irq);
        }
    }

    fn execute_interrupt(&mut self) {
        self.push_stack((self.pc >> 8) as u8); // Cycle 3
        self.push_stack(self.pc as u8); // Cycle 4

        let vector_loc = {
            let loc: u16;
            // The phi 1 of a cycle
            match &self.isr_running {
                Some(Interrupt::Irq) | Some(Interrupt::Nmi) => self.set_interrupts(),
                _ => {}
            }
            // Interrupt hijacking behavior
            if let Some(interrupt) = &self.interrupt_pending {
                loc = interrupt.get_vec_addr()
            } else {
                if let Some(interrupt) = &self.isr_running {
                    loc = interrupt.get_vec_addr()
                } else {
                    loc = Interrupt::IRQ_VECTOR
                }
            }
            // The interrupt sequences themselves do not perform interrupt polling, meaning at least one instruction from the interrupt handler will execute before another interrupt is serviced.
            // But we need to perform the set interrupts function, the changes should be undone
            match &self.isr_running {
                Some(Interrupt::Irq) | Some(Interrupt::Nmi) => self.interrupt_pending = None,
                _ => {}
            }
            loc
        };

        let mut status = self.status;
        match self.isr_running {
            Some(_) => status.set(Status::B, false),
            None => status.set(Status::B, true),
        }

        self.push_stack(status.bits());

        // Set PCL
        self.pc = utils::set_lower(self.pc, self.read(vector_loc));
        self.status.set(Status::I, true);
        // Set PCH
        self.pc = utils::set_upper(self.pc, self.read(vector_loc + 1));

        self.isr_running = None;
    }

    /// Handles NMI and IRQ interrupts, so they execute as the next instruction
    fn handle_interrupt(&mut self) {
        self.isr_running = self.interrupt_pending.take();
    }

    // pub fn adc(&mut self, mode: AddressingMode) {
    //     config_modes! {
    //         mode, pc, self.cycles;
    //         Immediate(2),
    //         ZeroPage(3),
    //         ZeroPageX(4),
    //         Absolute(4),
    //         AbsoluteX(4, true),
    //         AbsoluteY(4, true),
    //         IndirectX(6),
    //         IndirectY(5, true),
    //     }

    //     let val = self.get_data(mode, pc);

    //     // TODO: Replace with carrying_add when it gets stabalized
    //     let (s1, c1) = self
    //         .acc
    //         .overflowing_add(self.status.contains(Status::C) as u8);
    //     let (s2, c2) = val.overflowing_add(s1);

    //     self.status.set(Status::C, c1 | c2);
    //     self.status.set(
    //         Status::V,
    //         // Checks if the signs changed if the sign of the operand and acc were the same
    //         ((s2 & 0x80) != (self.acc & 0x80)) && ((s2 & 0x80) != (val & 0x80)),
    //     );

    //     self.acc = s2;

    //     self.status.set(Status::Z, self.acc == 0);
    //     self.status.set(Status::N, self.acc & (1 << 7) != 0);
    // }
}
