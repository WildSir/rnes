use self::{AddressingMode::*, Instruction::*};
use crate::{cpu::Status, Cpu};

/// Specifies several ways memory locations can be addressed by instructions.
#[derive(Debug)]
pub enum AddressingMode {
    /// Information to be manipulaed is implied by the instruction.
    Implicit,
    /// Operates directly on the accumulator.
    Accumulator,
    /// Operate directly on a constant supplied as an operand to the instruction.
    Immediate,
    /// Operates directly on the address supplied by the operand on zero page (0x00-0xFF).
    ZeroPage,
    /// Same as [ZeroPage](AddressingMode::ZeroPage), but adds the value of the X register to the address supplied, wrapping if nesscessary.
    ZeroPageX,
    /// Same as [ZeroPageX](AddressingMode::ZeroPageX), but operates on the Y register instead of X.
    ZeroPageY,
    /// Operand is added to program counter if condition is true. Since the program counter is incremented by +2 regardless of the results, the target instruction must be from -126 to +129.
    Relative,
    /// Operates on a full 16-bit address.
    Absolute,
    /// Adds the contents of the X register to the address supplied.
    AbsoluteX,
    /// Adds the contents of the Y register to the address supplied.
    AbsoluteY,
    /// 16-bit address indentifies the address of the LSB of another address where the data can be found.
    Indirect,
    /// Also known as pre-indexed, takes operand and adds it to the value of the X register (with zero page wraparound), to point to the the LSB of the target address.
    IndirectX,
    /// Also known as post-indexed, same as [IndexedIndirect](AddressingMode::IndexedIndirect), but operates on the value of the Y register instead.
    IndirectY,
}

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Instruction {
    //===========================
    // Load/Store Operations
    //===========================
    /// Load Accumulator
    LDA,
    /// Load X Register
    LDX,
    /// Load Y Register
    LDY,
    /// Store Accumulator
    STA,
    /// Store X Register
    STX,
    /// Store Y Register
    STY,

    //===========================
    // Register Transfers
    //===========================
    /// Transfer accumulator to X
    TAX,
    /// Transfer accumulator to Y
    TAY,
    /// Transfer X to accumulator
    TXA,
    /// Transfer Y to accumulator
    TYA,

    //===========================
    // Stack Operations
    //===========================
    /// Transfer stack pointer to X
    TSX,
    /// Transfer X to stack pointer
    TXS,
    /// Push accumulator on stack
    PHA,
    /// Push processor status on stack
    PHP,
    /// Pull accumulator from stack
    PLA,
    /// Pull processor status from stack
    PLP,

    //===========================
    // Logical
    //===========================
    /// Logical AND
    AND,
    /// Exclusive OR
    EOR,
    /// Logical Inclusive OR
    ORA,
    /// Bit Test
    BIT,

    //===========================
    // Arithmetic
    //===========================
    /// Add with Carry
    ADC,
    /// Subtract with Carry
    SBC,
    /// Compare accumulator
    CMP,
    /// Compare X register
    CPX,
    /// Compare Y register
    CPY,

    //===========================
    // Increments and Decrements
    //===========================
    /// Increment a memory location
    INC,
    /// Increment the X register
    INX,
    /// Increment the Y register
    INY,
    /// Decrement a memory location
    DEC,
    /// Decrement the X register
    DEX,
    /// Decrement the Y register
    DEY,

    //===========================
    // Shifts
    //===========================
    /// Arithmetic Shift Left
    ASL,
    /// Logical Shift Right
    LSR,
    /// Rotate Left
    ROL,
    /// Rotate Right
    ROR,

    //===========================
    // Jumps and Calls
    //===========================
    /// Jump to another location
    JMP,
    /// Jump to a subroutine
    JSR,
    /// Return from subroutine
    RTS,

    //===========================
    // Branches
    //===========================
    /// Branch if carry flag clear
    BCC,
    /// Branch if carry flag set
    BCS,
    /// Branch if zero flag set
    BEQ,
    /// Branch if negative flag set
    BMI,
    /// Branch if zero flag clear
    BNE,
    /// Branch negative flag clear
    BPL,
    /// Branch if overflow flag clear
    BVC,
    /// Branch if overflow flag set
    BVS,

    //===========================
    // Status Flag Changes
    //===========================
    /// Clear carry flag
    CLC,
    /// Clear decimal mode flag
    CLD,
    /// Clear interrupt disable flag
    CLI,
    /// Clear overflow flag
    CLV,
    /// Set carry flag
    SEC,
    /// Set decimal mode flag
    SED,
    /// Set interrupt disable flag
    SEI,

    //===========================
    // System Function
    //===========================
    /// Force an interrupt
    BRK,
    /// No Operation
    NOP,
    /// Return from Interrupt
    RTI,

    /// Invalid opcode
    XXX,
}

impl Cpu {
    #[rustfmt::skip]
    const INSTRUCTIONS: [(u8, Instruction, AddressingMode); 256] =
    [
        (0x00, BRK, Implicit), (0x01, ORA, IndirectX), (0x02, XXX, Implicit), (0x03, XXX, Implicit), (0x04, XXX, Implicit), (0x05, ORA, ZeroPage), (0x06, ASL, ZeroPage), (0x07, XXX, Implicit), (0x08, PHP, Implicit), (0x09, ORA, Immediate), (0x0A, ASL, Accumulator), (0x0B, XXX, Implicit), (0x0C, XXX, Implicit), (0x0D, ORA, Absolute), (0x0E, ASL, Absolute), (0x0F, XXX, Implicit),
        (0x10, BPL, Relative), (0x11, ORA, IndirectY), (0x12, XXX, Implicit), (0x13, XXX, Implicit), (0x14, XXX, Implicit), (0x15, ORA, ZeroPageX), (0x16, ASL, ZeroPageX), (0x17, XXX, Implicit), (0x18, CLC, Implicit), (0x19, ORA, AbsoluteY), (0x1A, XXX, Implicit), (0x1B, XXX, Implicit), (0x1C, XXX, Implicit), (0x1D, XXX, AbsoluteX), (0x1E, ASL, AbsoluteX), (0x1F, XXX, Implicit),
        (0x20, JSR, Absolute), (0x21, AND, IndirectX), (0x22, XXX, Implicit), (0x23, XXX, Implicit), (0x24, BIT, ZeroPage), (0x25, AND, ZeroPage), (0x26, ROL, ZeroPage), (0x27, XXX, Implicit), (0x28, PLP, Implicit), (0x29, AND, Immediate), (0x2A, ROL, Accumulator), (0x2B, XXX, Implicit), (0x2C, BIT, Absolute), (0x2D, AND, Absolute), (0x2E, ROL, Absolute), (0x2F, XXX, Implicit),
        (0x30, BMI, Relative), (0x31, AND, IndirectY), (0x32, XXX, Implicit), (0x33, XXX, Implicit), (0x34, XXX, Implicit), (0x35, AND, ZeroPageX), (0x36, ROL, ZeroPageX), (0x37, XXX, Implicit), (0x38, SEC, Implicit), (0x39, AND, AbsoluteY), (0x3A, XXX, Implicit), (0x3B, XXX, Implicit), (0x3C, XXX, Implicit), (0x3D, AND, AbsoluteX), (0x3E, ROL, AbsoluteX), (0x3F, XXX, Implicit),
        (0x40, RTI, Implicit), (0x41, EOR, IndirectX), (0x42, XXX, Implicit), (0x43, XXX, Implicit), (0x44, XXX, Implicit), (0x45, EOR, ZeroPage), (0x46, LSR, ZeroPage), (0x47, XXX, Implicit), (0x48, PHA, Implicit), (0x49, EOR, Immediate), (0x4A, LSR, Accumulator), (0x4B, XXX, Implicit), (0x4C, JMP, Absolute), (0x4D, EOR, Absolute), (0x4E, LSR, Absolute), (0x4F, XXX, Implicit),
        (0x50, BVC, Relative), (0x51, EOR, IndirectY), (0x52, XXX, Implicit), (0x53, XXX, Implicit), (0x54, XXX, Implicit), (0x55, EOR, ZeroPageX), (0x56, LSR, AbsoluteX), (0x57, XXX, Implicit), (0x58, CLI, Implicit), (0x59, EOR, AbsoluteY), (0x5A, XXX, Implicit), (0x5B, XXX, Implicit), (0x5C, XXX, Implicit), (0x5D, EOR, AbsoluteX), (0x5E, LSR, AbsoluteX), (0x5F, XXX, Implicit),
        (0x60, RTS, Implicit), (0x61, ADC, IndirectX), (0x62, XXX, Implicit), (0x63, XXX, Implicit), (0x64, XXX, Implicit), (0x65, ADC, ZeroPage), (0x66, ROR, ZeroPage), (0x67, XXX, Implicit), (0x68, CLI, Implicit), (0x69, EOR, AbsoluteY), (0x6A, ROR, Accumulator), (0x6B, XXX, Implicit), (0x6C, JMP, Indirect), (0x6D, ADC, Absolute), (0x6E, ROR, Absolute), (0x6F, XXX, Implicit),
        (0x70, BVS, Relative), (0x71, ADC, IndirectY), (0x72, XXX, Implicit), (0x73, XXX, Implicit), (0x74, XXX, Implicit), (0x75, ADC, ZeroPageX), (0x76, ROR, ZeroPageX), (0x77, XXX, Implicit), (0x78, SEI, Implicit), (0x79, ADC, AbsoluteY), (0x7A, XXX, Implicit), (0x7B, XXX, Implicit), (0x7C, XXX, Implicit), (0x7D, ADC, AbsoluteX), (0x7E, ROR, AbsoluteX), (0x7F, XXX, Implicit),
        (0x80, XXX, Implicit), (0x81, STA, IndirectX), (0x82, XXX, Implicit), (0x83, XXX, Implicit), (0x84, STY, ZeroPage), (0x85, STA, ZeroPage), (0x86, STX, ZeroPage), (0x87, XXX, Implicit), (0x88, DEY, Implicit), (0x89, XXX, Implicit), (0x8A, TXA, Implicit), (0x8B, XXX, Implicit), (0x8C, STY, Absolute), (0x8D, STA, Absolute), (0x8E, STX, Absolute), (0x8F, XXX, Implicit),
        (0x90, BCC, Relative), (0x91, STA, IndirectY), (0x92, XXX, Implicit), (0x93, XXX, Implicit), (0x94, STY, ZeroPageX), (0x95, STA, ZeroPageX), (0x96, STX, ZeroPageX), (0x97, XXX, Implicit), (0x98, DEY, Implicit), (0x99, STA, AbsoluteY), (0x9A, TXS, Implicit), (0x9B, XXX, Implicit), (0x9C, XXX, Implicit), (0x9D, STA, AbsoluteX), (0x9E, XXX, Implicit), (0x9F, XXX, Implicit),
        (0xA0, LDY, Immediate), (0xA1, LDA, AbsoluteX), (0xA2, LDX, Immediate), (0xA3, XXX, Implicit), (0xA4, LDY, ZeroPage), (0xA5, LDA, ZeroPage), (0xA6, LDX, ZeroPage), (0xA7, XXX, Implicit), (0xA8, DEY, Implicit), (0xA9, LDA, Immediate), (0xAA, TAX, Implicit), (0xAB, XXX, Implicit), (0xAC, LDY, Absolute), (0xAD, LDA, Absolute), (0xAE, LDX, Absolute), (0xAF, XXX, Implicit),
        (0xB0, BCS, Relative), (0xB1, LDA, IndirectY), (0xB2, XXX, Implicit), (0xB3, XXX, Implicit), (0xB4, LDY, ZeroPageX), (0xB5, LDA, ZeroPageX), (0xB6, LDX, ZeroPageY), (0xB7, XXX, Implicit), (0xB8, CLV, Implicit), (0xB9, LDA, AbsoluteY), (0xBA, TSX, Implicit), (0xBB, XXX, Implicit), (0xBC, LDY, AbsoluteX), (0xBD, LDA, AbsoluteX), (0xBE, LDX, AbsoluteY), (0xBF, XXX, Implicit),
        (0xC0, CPY, Immediate), (0xC1, CMP, IndirectX), (0xC2, XXX, Implicit), (0xC3, XXX, Implicit), (0xC4, CPY, ZeroPage), (0xC5, CMP, ZeroPage), (0xC6, DEC, ZeroPage), (0xC7, XXX, Implicit), (0xC8, INY, Implicit), (0xC9, CMP, Immediate), (0xCA, DEX, Implicit), (0xCB, XXX, Implicit), (0xCC, CPY, Absolute), (0xCD, CMP, Absolute), (0xCE, DEC, Absolute), (0xCF, XXX, Implicit),
        (0xD0, BNE, Relative), (0xD1, CMP, IndirectY), (0xD2, XXX, Implicit), (0xD3, XXX, Implicit), (0xD4, XXX, Implicit), (0xD5, CMP, ZeroPageX), (0xD6, DEC, ZeroPageX), (0xD7, XXX, Implicit), (0xD8, CLD, Implicit), (0xD9, CMP, AbsoluteY), (0xDA, XXX, Implicit), (0xDB, XXX, Implicit), (0xDC, XXX, Implicit), (0xDD, CMP, AbsoluteX), (0xDF, DEC, AbsoluteX), (0xDF, XXX, Implicit),
        (0xE0, CPX, Immediate), (0xE1, SBC, IndirectX), (0xE2, XXX, Implicit), (0xE3, XXX, Implicit), (0xE4, CPX, ZeroPage), (0xE5, SBC, ZeroPage), (0xE6, INC, ZeroPage), (0xE7, XXX, Implicit), (0xE8, INX, Implicit), (0xE9, SBC, Immediate), (0xEA, NOP, Implicit), (0xEB, XXX, Implicit), (0xEC, CPX, Absolute), (0xED, SBC, Absolute), (0xEE, INC, Absolute), (0xEF, XXX, Implicit),
        (0xF0, BEQ, Relative), (0xF1, SBC, IndirectY), (0xF2, XXX, Implicit), (0xF3, XXX, Implicit), (0xF4, XXX, Implicit), (0xF5, SBC, ZeroPageX), (0xF6, INC, ZeroPageX), (0xF7, XXX, Implicit), (0xF8, SED, Implicit), (0xF9, SBC, AbsoluteY), (0xFA, XXX, Implicit), (0xFB, XXX, Implicit), (0xFC, XXX, Implicit), (0xFD, SBC, AbsoluteX), (0xFE, INC, AbsoluteX), (0xFF, XXX, Implicit),
    ];
    pub fn lookup_opcode(&mut self, opcode: u8) {
        let instruction = Self::INSTRUCTIONS
            .into_iter()
            .find(|e| e.0 == opcode)
            .unwrap();
        self.instruction_reg = Some(instruction.1);
        self.addr_mode_reg = Some(instruction.2);
    }

    fn set_zn(&mut self, val: u8) {
        self.status.set(Status::Z, val == 0);
        self.status.set(Status::N, (val as i8) < 0);
    }

    pub(super) fn lda(&mut self) {
        self.acc = self.fetch_data();
        self.set_zn(self.acc);
    }

    pub(super) fn ldx(&mut self) {
        self.x = self.fetch_data();
        self.set_zn(self.x);
    }

    pub(super) fn ldy(&mut self) {
        self.y = self.fetch_data();
        self.set_zn(self.y);
    }
    pub(super) fn sta(&mut self) {
        self.write_data(self.acc);
    }

    pub(super) fn stx(&mut self) {
        self.write_data(self.x);
    }

    pub(super) fn sty(&mut self) {
        self.write_data(self.y);
    }

    pub(super) fn tax(&mut self) {
        self.x = self.acc;
        self.set_zn(self.x);
    }

    pub(super) fn tay(&mut self) {
        self.y = self.acc;
        self.set_zn(self.y);
    }

    pub(super) fn txa(&mut self) {
        self.acc = self.x;
        self.set_zn(self.acc);
    }

    pub(super) fn tya(&mut self) {
        self.acc = self.y;
        self.set_zn(self.acc);
    }

    pub(super) fn tsx(&mut self) {
        self.x = self.sp;
        self.set_zn(self.x);
    }

    pub(super) fn txs(&mut self) {
        self.sp = self.x;
    }

    pub(super) fn pha(&mut self) {
        todo!()
    }

    pub(super) fn brk(&mut self) {
        self.execute_interrupt();
    }

    pub(super) fn rti(&mut self) {
        self.pull_stack();
    }
}
