use log::info;

use crate::{
    cpu::Cycle,
    mapper::{nrom::Nrom, Mapper, MapperTrait},
};

#[derive(Debug)]
pub struct Bus {
    /// Physical NES RAM
    ram: [u8; 0x800],
    pub mapper: Mapper,
    pub addr_bus: u16,
    pub data_bus: u8,
    pub control_bus: Cycle,
}

impl Bus {
    pub fn new() -> Self {
        // TODO: Possibly implement random initial memory state (and bus state?)
        Bus {
            ram: [0; 0x800],
            mapper: Mapper::from(Nrom::new()),
            addr_bus: 0,
            data_bus: 0,
            control_bus: Cycle::Read,
        }
    }

    /// Runs the appropiate operation based on the values on the address, data, and control buses
    pub fn request_raw(&mut self) {
        match self.control_bus {
            Cycle::Read => {
                self.data_bus = match self.addr_bus {
                    // RAM, Stack, and Zero Page
                    0x0..=0x1FFF => self.ram[(self.addr_bus & 0x7FF) as usize],

                    // TODO: PPU registers memory map

                    // Should look something like this
                    // 0x2000..=0x3FFF => self.ppu_regs[(addr & 0x2007)]
                    0x2000..=0x3FFF => unimplemented!("PPU is not implemented yet"),

                    // I/O Registers
                    0x4000..=0x4013 => self.data_bus,
                    0x4014 => self.data_bus,
                    0x4015 => unimplemented!("APU is not implemented yet"),
                    // TODO: Joystick support
                    0x4016 => unimplemented!("Joystick support is not implemented yet"),
                    0x4017 => unimplemented!("Joystick support is not implemented yet"),
                    0x4018..=0x401F => {
                        info!("CPU Test Mode is not enabled (tried to access memory region)");
                        self.data_bus
                    }

                    // Unmapped; for mapper use
                    0x4020..=0xFFFF => {
                        self.mapper.read(self.addr_bus, &mut self.data_bus);
                        self.data_bus
                    }
                };
            }
            Cycle::Write => {
                match self.addr_bus {
                    0x0..=0x1FFF => self.ram[(self.addr_bus & 0x7FF) as usize] = self.data_bus,
                    // TODO: PPU registers memory map
                    0x2000..=0x3FFF => unimplemented!("PPU is not the implemented yet"),
                    0x4000..=0x4013 => unimplemented!("APU is not implemented yet"),
                    0x4014 => unimplemented!("PPU is not the implemented yet"),
                    0x4015 => unimplemented!("APU is not implemented yet"),
                    0x4016 => unimplemented!("Joystick support is not implemented yet"),
                    0x4017 => unimplemented!("APU is not implemented yet"),
                    0x4018..=0x401F => {
                        info!("CPU Test Mode is not enabled (tried to access memory region)")
                    }

                    // Unmapped; for mapper use
                    0x4020..=0xFFFF => self.mapper.write(self.addr_bus, &mut self.data_bus),
                }
            }
        };
    }

    pub fn addr_msb(&mut self) -> u8 {
        (self.addr_bus >> 7) as u8
    }

    pub fn set_addr_lsb(&mut self, val: u8) {
        self.addr_bus = self.addr_bus & (0xFF00 | val as u16);
    }

    pub fn set_addr_msb(&mut self, val: u8) {
        self.addr_bus = self.addr_bus & (((val as u16) << 7) | 0x00FF);
    }
}

impl Default for Bus {
    fn default() -> Self {
        Bus::new()
    }
}
