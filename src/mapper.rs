use enum_dispatch::enum_dispatch;

use crate::cpu::Cycle;

use self::nrom::Nrom;

pub mod nrom;

#[enum_dispatch]
pub trait MapperTrait: std::fmt::Debug {
    fn request_raw(&mut self, addr_bus: u16, data_bus: &mut u8, control_bus: Cycle);
    fn read(&mut self, addr_bus: u16, data_bus: &mut u8) {
        self.request_raw(addr_bus, data_bus, Cycle::Read);
    }
    fn write(&mut self, addr_bus: u16, data_bus: &mut u8) {
        self.request_raw(addr_bus, data_bus, Cycle::Write);
    }
}

#[enum_dispatch(MapperTrait)]
#[derive(Debug)]
pub enum Mapper {
    Nrom,
}
